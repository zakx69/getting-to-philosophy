package com.bento.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.bento.utils.WikipediaPageValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spark.utils.IOUtils;

public class WikipediaPageParserTest {

    @Mock
    private WikipediaPageValidator wikipediaPageValidator;
    private WikipediaPageParser wikipediaPageParser;
    private Document document;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        wikipediaPageParser = new WikipediaPageParser(wikipediaPageValidator);
        document = Jsoup.parse(IOUtils.toString(this.getClass().getResourceAsStream("/html/gettingToPhilosophy.html")));

        when(wikipediaPageValidator.isValidWikiLink(anyString())).thenReturn(true);
    }

    @Test
    public void getTitleTest() {
        assertEquals("Wikipedia:Getting to Philosophy", wikipediaPageParser.getTitle(document));
    }

    @Test
    public void getFirstValidLinkTest() {
        assertEquals("/wiki/Point_and_click", wikipediaPageParser.getFirstValidLink(document));
    }

    @Test
    public void getFirstValidLinkTest_FirstNotValid() {
        when(wikipediaPageValidator.isValidWikiLink(eq("/wiki/Point_and_click"))).thenReturn(false);
        assertEquals("/wiki/Help:Link", wikipediaPageParser.getFirstValidLink(document));
    }

    @Test
    public void getFirstValidLinkTest_NoValidLinks() {
        when(wikipediaPageValidator.isValidWikiLink(anyString())).thenReturn(false);
        assertNull(wikipediaPageParser.getFirstValidLink(document));
    }
}
