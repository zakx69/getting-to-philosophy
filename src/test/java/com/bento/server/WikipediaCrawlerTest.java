package com.bento.server;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import com.bento.persistence.WikipediaPath;
import com.bento.persistence.WikipediaPathDao;
import com.bento.utils.WikipediaPageValidator;
import java.util.Optional;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spark.utils.IOUtils;

public class WikipediaCrawlerTest {

    @Mock
    private WikipediaPageValidator wikipediaPageValidator;
    @Mock
    private WikipediaPageParser wikipediaPageParser;
    @Mock
    private WikipediaPathDao wikipediaPathDao;

    private WikipediaCrawler wikipediaCrawler;
    private Document document;
    private String startLink = "https://en.wikipedia.org/wiki/Wikipedia:Getting_to_Philosophy";
    private String startLinkRelative = "/wiki/Wikipedia:Getting_to_Philosophy";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        String philosophyLink = "http://en.wikipedia.org/wiki/philosophy";
        wikipediaCrawler = new WikipediaCrawler(wikipediaPageValidator, wikipediaPageParser, wikipediaPathDao);
        document = Jsoup.parse(IOUtils.toString(this.getClass().getResourceAsStream("/html/crawler.html")));

        when(wikipediaPageValidator.isValidWikiLink(anyString())).thenReturn(true);
        when(wikipediaPageParser.getFirstValidLink(any())).thenReturn(philosophyLink);
        when(wikipediaPageParser.getTitle(any())).thenReturn("title");
        when(wikipediaPathDao.select(anyString())).thenReturn(Optional.empty());
    }

    @Test
    public void getPathToPhilosophyTest() {
        wikipediaCrawler.getPathToPhilosophy(startLink, document);

        String expectedTitle = "Path from Wikipedia:Getting_to_Philosophy to Philosophy:";
        String expectedStatus = "<b>Status: </b>Link to philosophy founded after 1 iterations";
        String expectedPath = "<b>Path: </b><br>Wikipedia:Getting_to_Philosophy -&gt; philosophy";
        assertEquals(expectedTitle, document.select(".title").first().html());
        assertEquals(expectedStatus, document.select(".status").first().html());
        assertEquals(expectedPath, document.select(".path").first().html());
    }

    @Test
    public void getPathToPhilosophyTest_WithDatabaseMatch() {
        when(wikipediaPathDao.select(anyString())).thenReturn(Optional.of(WikipediaPath.builder().build()));

        wikipediaCrawler.getPathToPhilosophy(startLink, document);

        verifyZeroInteractions(wikipediaPageParser);
    }

    @Test
    public void getPathToPhilosophyTest_RelativeLink() {
        wikipediaCrawler.getPathToPhilosophy(startLinkRelative, document);

        String expectedTitle = "Path from Wikipedia:Getting_to_Philosophy to Philosophy:";
        String expectedStatus = "<b>Status: </b>Link to philosophy founded after 1 iterations";
        String expectedPath = "<b>Path: </b><br>Wikipedia:Getting_to_Philosophy -&gt; philosophy";
        assertEquals(expectedTitle, document.select(".title").first().html());
        assertEquals(expectedStatus, document.select(".status").first().html());
        assertEquals(expectedPath, document.select(".path").first().html());
    }

    @Test
    public void getPathToPhilosophyTest_NoLinks() {
        when(wikipediaPageParser.getFirstValidLink(any())).thenReturn(null);

        wikipediaCrawler.getPathToPhilosophy(startLink, document);

        String expectedTitle = "Path from Wikipedia:Getting_to_Philosophy to Philosophy:";
        String expectedStatus = "<b>Status: </b>Cycle reached.";
        String expectedPath = "<b>Path: </b><br>Wikipedia:Getting_to_Philosophy";
        assertEquals(expectedTitle, document.select(".title").first().html());
        assertEquals(expectedStatus, document.select(".status").first().html());
        assertEquals(expectedPath, document.select(".path").first().html());
    }

    @Test
    public void getPathToPhilosophyTest_InvalidLink() {
        when(wikipediaPageValidator.isValidWikiLink(anyString())).thenReturn(false);

        String response = wikipediaCrawler.getPathToPhilosophy("http://not.a.valid.link", document);

        String expected = "Please specify a valid wiki link!";
        assertEquals(expected, response);
    }

}
