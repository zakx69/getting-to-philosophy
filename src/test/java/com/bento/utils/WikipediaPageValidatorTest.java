package com.bento.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class WikipediaPageValidatorTest {

    private WikipediaPageValidator wikipediaPageValidator;

    @Before
    public void setUp() {
        wikipediaPageValidator = new WikipediaPageValidator();
    }

    @Test
    public void absoluteWikiTest() {
        String link = "https://en.wikipedia.org/wiki/Wikipedia:Getting_to_Philosophy";
        assertTrue(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void absoluteWikiTest_2() {
        String link = "https://en.wikipedia.org/wiki/Philosophy";
        assertTrue(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void absoluteWikiTest_Failure() {
        String link = "https://en.wikipedia.org/wika/Wikipedia:Getting_to_Philosophy";
        assertFalse(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void absoluteWikiTest_Failure_2() {
        String link = "https://en.wikipedia.org/wiki/";
        assertFalse(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void relativeWikiTest() {
        String link = "/wiki/Something_you_dont_know";
        assertTrue(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void relativeWikiTest_Failure() {
        String link = "/wikiiiii/Something";
        assertFalse(wikipediaPageValidator.isValidWikiLink(link));
    }

    @Test
    public void relativeWikiTest_Failure_2() {
        String link = "/wik";
        assertFalse(wikipediaPageValidator.isValidWikiLink(link));
    }

}
