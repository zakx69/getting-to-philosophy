package com.bento;

import com.bento.controller.SparkAppController;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class SparkApp {

    private static final int PORT = 8080;

    public static void main(String[] args) {
        log.info("Starting Spark server on port {}...", PORT);

        // Create a dummy injector to use Google Guice
        Injector injector = Guice.createInjector((Module) binder -> {});
        SparkAppController controller = injector.getBinding(SparkAppController.class).getProvider().get();
        controller.addControllers(PORT);

        log.info("Spark server started successfully.");
    }

}
