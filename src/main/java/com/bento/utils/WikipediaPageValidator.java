package com.bento.utils;

import com.google.inject.Singleton;

@Singleton
public class WikipediaPageValidator {

    /**
     * Checks for a link to be a valid wiki page, either in it's absolute or relative form.
     *
     * @param link - Link to validate
     * @return Boolean with the result of the validation
     */
    public boolean isValidWikiLink(String link) {
        return link.matches("^https?://([\\w]+.)wikipedia.org/wiki/.+")
                || link.matches("^/wiki/.+");
    }
}
