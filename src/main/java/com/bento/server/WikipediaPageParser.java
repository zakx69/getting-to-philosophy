package com.bento.server;

import com.bento.utils.WikipediaPageValidator;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Slf4j
@Singleton
public class WikipediaPageParser {

    private WikipediaPageValidator wikipediaPageValidator;

    @Inject
    public WikipediaPageParser(WikipediaPageValidator wikipediaPageValidator) {
        this.wikipediaPageValidator = wikipediaPageValidator;
    }

    /**
     * Retrieves the title of the document
     *
     * @param document - HTML document
     * @return Document title
     */
    public String getTitle(Document document) {
        return document.select("#firstHeading").html();
    }

    /**
     * Parses the document and retrieves a valid link that:
     * - It's non-parenthesized, non-italicized
     * - It's not an external links, link to the current page, or red links (links to non-existent pages)
     *
     * @param document - HTML document
     * @return First valid link found
     */
    public String getFirstValidLink(Document document) {
        String currentTitle = getTitle(document);
        // Select the paragraphs inside the main section
        Elements select = document.select("#mw-content-text p");
        for (Element element : select) {
            String paragraph = element.html();
            // Remove links inside parenthesis and everything inside italics.
            // Note: Links can have parenthesis in their titles.
            paragraph = paragraph.replaceAll("\\(.*?<a>.*?</a>.*?\\)", "");
            paragraph = paragraph.replaceAll("<i>.*?</i>", "");
            Document paragraphDocument = Jsoup.parse(paragraph);

            // Search for links once rules have been applied
            Elements links = paragraphDocument.select("a");
            for (Element link : links) {
                // If it's a red link. Check https://en.wikipedia.org/wiki/Wikipedia:Red_link
                if (link.attr("class").equals("new")) continue;

                String linkString = link.attr("href");
                String title = link.attr("title").replaceAll("_", " ");
                // If the title is the same then continue searching
                if (currentTitle.equals(title)) continue;

                String html = link.outerHtml();
                log.debug("url: {}, title: {}, html: {}", link, title, html);
                if (wikipediaPageValidator.isValidWikiLink(linkString)) {
                    return linkString;
                }
            }
        }

        return null;
    }
}
