package com.bento.server;

import com.bento.persistence.WikipediaPath;
import com.bento.persistence.WikipediaPathDao;
import com.bento.utils.WikipediaPageValidator;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Slf4j
@Singleton
public class WikipediaCrawler {

    private final int MAX_ITERATIONS = 100;
    private final String HTTP_EN_WIKIPEDIA_ORG_WIKI_PHILOSOPHY = "http://en.wikipedia.org/wiki/philosophy";
    private final String HTTP_EN_WIKIPEDIA_ORG = "http://en.wikipedia.org";

    private WikipediaPageValidator wikipediaPageValidator;
    private WikipediaPageParser wikipediaPageParser;
    private WikipediaPathDao wikipediaPathDao;

    @Inject
    public WikipediaCrawler(WikipediaPageValidator wikipediaPageValidator, WikipediaPageParser wikipediaPageParser, WikipediaPathDao wikipediaPathDao) {
        this.wikipediaPageValidator = wikipediaPageValidator;
        this.wikipediaPageParser = wikipediaPageParser;
        this.wikipediaPathDao = wikipediaPathDao;
    }

    /**
     * Entry point for getting the path to wiki Philosophy page
     *
     * @param startingLink - Link to the first page to crawl
     * @param document     - Document to fill in the response
     * @return String with the path followed
     */
    public String getPathToPhilosophy(String startingLink, Document document) {
        startingLink = convertLinkToAbsolute(startingLink);
        // We will be using http protocol as it requires less amount of processing (no TSL)
        startingLink = startingLink.replace("https://", "http://");
        if (!wikipediaPageValidator.isValidWikiLink(startingLink)) {
            log.info("Invalid link: {}", startingLink);
            return "Please specify a valid wiki link!";
        }

        // Get the title without parsing the page
        String startingTitle = startingLink.substring(29);
        String path;

        log.info("Received url: {}", startingLink);
        Optional<WikipediaPath> wikipediaPathOptional = wikipediaPathDao.select(startingTitle);
        if (wikipediaPathOptional.isPresent()) {
            WikipediaPath wikipediaPath = wikipediaPathOptional.get();
            log.debug("Link found in the database: {}, {}", startingTitle, wikipediaPath);
            return wikipediaPath.getDocument();
        }

        // Get the path until the cycle, max amount of iterations, or reaching Philosophy
        List<String> visitedLinks = processLinkInternal(startingLink);
        path = getTitlesPath(visitedLinks);

        String status;
        if (!visitedLinks.isEmpty() && visitedLinks.get(visitedLinks.size() - 1).equalsIgnoreCase(HTTP_EN_WIKIPEDIA_ORG_WIKI_PHILOSOPHY)) {
            status = String.format("Link to philosophy founded after %d iterations", visitedLinks.size() - 1);
            log.info(status);
        } else if (visitedLinks.size() - 1 < MAX_ITERATIONS) {
            log.info("Cycle reached: {}", path);
            status = "Cycle reached.";
        } else {
            status = String.format("Link not found for philosophy after %d iterations", visitedLinks.size() - 1);
            log.warn(status);
        }

        // Fill the html response document
        fillCrawlDocument(startingTitle, status, path, document);

        // Save it into the database
        WikipediaPath wikipediaPath = WikipediaPath.builder().title(startingTitle).path(path).document(document.toString()).build();
        wikipediaPathDao.insert(wikipediaPath);

        return document.toString();
    }

    /**
     * Fills the document with the specified information.
     *
     * @param start    - Title indicating the starting wiki link
     * @param status   - Status of the crawler
     * @param path     - Path followed by the crawler
     * @param document - HTML document to fill
     */
    private void fillCrawlDocument(String start, String status, String path, Document document) {
        String title = String.format("Path from %s to Philosophy:", start);

        document.select(".title").html(title);
        document.select(".status").first().append(status);
        document.select(".path").first().append(path);
    }

    /**
     * Get the path of the titles visited.
     *
     * @param visitedLinks - List with all the links visited
     * @return Path
     */
    private String getTitlesPath(List<String> visitedLinks) {
        if (visitedLinks.isEmpty()) {
            return "";
        }

        // All links will start with 'http://en.wikipedia.org/wiki/' and will be followed by the title
        List<String> titles = visitedLinks.stream().map(link -> link.substring(29)).collect(Collectors.toList());
        return String.join(" -> ", titles);
    }

    /**
     * Gets the path followed while trying to reach Philosophy
     *
     * @param startingLink - Link to start with
     * @return List of links followed
     */
    private List<String> processLinkInternal(String startingLink) {
        List<String> visitedLinks = new ArrayList<>();

        String link = startingLink;
        Document document;
        int iterations = 0;
        while (iterations < MAX_ITERATIONS && !link.equalsIgnoreCase(HTTP_EN_WIKIPEDIA_ORG_WIKI_PHILOSOPHY) && !visitedLinks.contains(link)) {
            visitedLinks.add(link);

            // Get the html document for the link
            try {
                document = Jsoup.connect(link).get();
            } catch (IOException e) {
                log.error("Error creating http connection to url: {}", link);
                return Collections.emptyList();
            }

            String title = wikipediaPageParser.getTitle(document);
            link = wikipediaPageParser.getFirstValidLink(document);

            // If not links then stop
            if (StringUtils.isBlank(link)) {
                break;
            }

            // If the link is a relative one convert to absolute
            link = convertLinkToAbsolute(link);

            log.info("First link for \"{}\" in url is: {}", title, link);
            iterations++;
        }

        if (!StringUtils.isBlank(link)) {
            visitedLinks.add(link);
        }
        return visitedLinks;
    }

    /**
     * Converts a wikipedia link to absolute wikipedia link
     *
     * @param link - Link to convert
     * @return Absolute wikipedia link
     */
    private String convertLinkToAbsolute(String link) {
        if (link.startsWith("/wiki/")) {
            link = HTTP_EN_WIKIPEDIA_ORG + link;
        }
        return link;
    }

}
