package com.bento.controller;

import static spark.Spark.get;
import static spark.SparkBase.port;

import com.bento.server.WikipediaCrawler;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

/**
 * Add the rest controllers for the application.
 */
@Singleton
public class SparkAppController {

    private WikipediaCrawler wikipediaCrawler;

    @Inject
    public SparkAppController(WikipediaCrawler wikipediaCrawler) {
        this.wikipediaCrawler = wikipediaCrawler;
    }

    /**
     * Registers the REST controllers in the specified port.
     *
     * @param port - Port to listen
     */
    public void addControllers(int port) {
        port(port);

        get("/", (Request req, Response res) -> IOUtils.toString(this.getClass().getResourceAsStream("/html/index.html")));
        get("/crawler", (Request req, Response res) -> {
            Document document = Jsoup.parse(IOUtils.toString(this.getClass().getResourceAsStream("/html/crawler.html")));
            return wikipediaCrawler.getPathToPhilosophy(req.queryMap("url").value(), document);
        });
    }
}
