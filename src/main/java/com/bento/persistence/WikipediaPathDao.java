package com.bento.persistence;

import com.google.inject.Singleton;
import java.util.Optional;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

@Singleton
public class WikipediaPathDao {

    private Session session;

    public WikipediaPathDao() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(WikipediaPath.class);
        Configuration configure = configuration.configure();

        session = configure.buildSessionFactory().openSession();
    }

    /**
     * Gets the Wikipedia path if stored in the database
     *
     * @param title - Title from start
     * @return Optional of the Wikipedia path
     */
    public Optional<WikipediaPath> select(String title) {
        WikipediaPath wiki = session.get(WikipediaPath.class, title);
        return (wiki != null) ? Optional.of(wiki) : Optional.empty();
    }

    /**
     * Saves a Wikipedia path in the database
     *
     * @param wikipediaPath - Wikipedia Path object to save
     */
    public void insert(WikipediaPath wikipediaPath) {
        session.save(wikipediaPath);
    }
}
