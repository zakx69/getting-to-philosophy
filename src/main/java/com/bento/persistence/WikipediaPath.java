package com.bento.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Entity
@Table(name = "wikipedia_path")
public class WikipediaPath {
    @Id
    @Column(name = "title")
    private String title;
    @Column(name = "path")
    private String path;
    @Column(name = "document", length = 5000)
    private String document;
}
