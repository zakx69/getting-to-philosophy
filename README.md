# Getting to Philosophy

This java project consists of crawling wikipedia pages from a specific one until we reach either a cycle or 'Getting to Philosophy' link.

### Frameworks and libraries used
The main libraries used by the project are:
 - Gradle: version manager
 - Spark framework: REST api controller
 - Google Guice: dependency injection
 - Jsoup: Parsing HTML documents
 - Hibernate: ORM for Java
 - SQLite: simple database
 - Mockito: mocking classes

### How to run the service
Run the following command inside a terminal:
```bash
./gradlew run
```

It will output some configuration at the beginning such as the database creation and the REST controller configuration.
The controller will be listening at `localhost` in port `8080`.

As soon as you see this message, the server will be ready to receive requests:
```bash
[Thread-1] INFO org.eclipse.jetty.server.ServerConnector - Started ServerConnector@557fc129{HTTP/1.1}{0.0.0.0:8080}
```

### How to interact with the frontend
Open your preferred web browser and navigate to `localhost:8080`. Once it loads you will see a form in the page where you should input your absolute 
wikipedia URL to start the search. Once you input it, click submit.

This will generate a GET query that will be processed by the backend. Once the backend finishes, the frontend will refresh showing a new web page with the 
path founded. 

### Check unit tests coverage
This project makes use of Jacoco for determining the unit tests code coverage. For that you need to run:
```bash
./gradlew clean test jacocoTestReport
```

During it execution it will output the html file where you can get a more detailed view about the code coverage.
No integration or systems tests where done.
